<?php

namespace Drupal\group_member_import\Form;

use Drupal\Component\Utility\Environment;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Drupal\group_member_import\GroupMemberImportFields;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Link;


/**
 * Implements form to upload a file and start the batch on form submit.
 *
 * @see \Drupal\Core\Form\FormBase
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class GroupMemberImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'csvimport_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = null) {
 
    // Get options.
    if (!is_null($group) && is_object($group)) {
      $current_user = \Drupal::currentUser();
      $options_group_roles = [];
      $group_type = $group->getGroupType();
      $group_roles = \Drupal::entityTypeManager()->getStorage('group_role')->loadMultiple();      

      if (!empty($group_type) && !empty($group_roles)) {       
        /** @var  \Drupal\group\Entity\GroupRole $group_role */
        foreach ($group_roles as $role_id => $group_role) {
          if ($group_role->getGroupTypeId() == $group_type->id()) {
            if(!$group_role->isInternal()) {
              $options_group_roles[$role_id] = $group_role->label();           
            }
          }
        }
      }
    }    

    $config = \Drupal::service('config.factory')->get('group_member_import.settings');

    $options_user_roles = $config->get('allowed_roles');

    foreach($options_user_roles as $key => $role) {
      if ($key === 'authenticated' || $key === 'anonymous') {
        unset($options_user_roles[$key]);
      }
    }    

    // Build form.   
    $form['#attributes'] = [
      'enctype' => 'multipart/form-data',
    ];

    $form['howto'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('How to use'),      
    ];  

    // Url to config.
    $settings_url = Url::fromRoute('group_member_import.settings', [], ['absolute' => TRUE]);
    $settings_url->setOption('query', \Drupal::destination()->getAsArray());
    
    // Settings link
    $settings_link = Link::fromTextAndUrl($this->t('click here'), $settings_url)->toRenderable();
    $settings_link['#attributes'] = ['class' => ['action-link', 'action-link--icon-cog']];
    
    // Render the settings link
    $build_settings_link = \Drupal::service('renderer')->render($settings_link);

    // Help listing
    $help_items = [
      $this->t('Copy/paste your data from spreadsheet <b>OR</b> upload a CSV file'),
      $this->t('Users will be identified via <b>user.uid</b>, <b>user.mail</b> or <b>user.name</b> and created if needed'),
      $this->t('The file or copy/pasted text must contain headers that specify entity type, field name and (if applicable) array index, eg for address.'),
      $this->t('Administrators can @link to define available fields',['@link' => $build_settings_link])
    ];

    $build_help_list = [
      '#theme' => 'item_list',
      '#items' => $help_items,
    ];

    $form['howto']['help_for_file'] = [      
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => \Drupal::service('renderer')->render($build_help_list)
    ];

    $form['howto']['example_for_file'] = [                  
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('<pre>user.name;user.mail;profile.field_profile_first_name;profile.field_profile_last_name;profile.field_profile_date_of_birth;profile.field_profile_selection_eval<br>janedoe;janedoe@example.com;Jane;Doe;1970-05-05;A</pre><br>'),
    ];   
    
    $form['importdata'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Importdata') . ' <span class="form-required">*</span>',
      '#description' => $this->t('Please note: You have to either upload a file or text.')
    ];  

    $form['importdata']['csvcp'] = [
      '#title'            => $this->t('Spreadsheet copy/paste'),
      '#type'             => 'textarea',
      '#description'      => $this->t('Copy/paste from spreadsheet file. Please lookup your current delimiter setting.'),      
    ];

    $form['importdata']['csvfile'] = [
      '#title'            => $this->t('CSV file'),
      '#type'             => 'file',
      '#description'      => ($max_size = Environment::getUploadMaxSize()) ? $this->t('Due to server restrictions, the <strong>maximum upload file size is @max_size</strong>. Files that exceed this size will be disregarded.', ['@max_size' => format_size($max_size)]) : '',
      '#element_validate' => ['::validateFileupload'],
    ];    

    $form['importdata']['delimiter'] = array(
      '#title' => $this->t('CSV delimiter'),
      '#type' => 'select',
      '#options' => [';', ',', 'TAB'],     
    );            

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),      
    ];  

    $form['settings']['group'] = array(
      '#title' => $this->t('Group'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'group',
      '#default_value' => $group, // The #default_value can be either an entity object or an array of entity objects.
      '#disabled' => TRUE
    );    

    $form['settings']['enrol'] = [                  
      '#type' => 'checkbox',      
      '#title' => $this->t('Enrol users into this group'),
      '#description' => $this->t('Uncheck to only import user data and not add as group members.'),
      '#default_value' => true,
    ];        

    $form['settings']['group_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Assign group roles'),
      '#options' => $options_group_roles,
    );

    if ($options_user_roles) {
      $form['settings']['user_roles'] = array(
        '#type' => 'checkboxes',
        '#title' => $this->t('Assign user roles'),
        '#options' => $options_user_roles,      
      );
    }

    $form['settings']['overwrite_passwords'] = [                  
      '#type' => 'checkbox',      
      '#title' => $this->t('Change password for existing users'),
      '#description' => $this->t('Password can be specified by providing a column "user.pass". By default, it only applies to newly created users.'),
    ]; 

    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Start Import'),
      '#attributes' => [
        'class' => ['button', 'btn','btn-primary'],
      ],
    ];

    // Attach LIbrary
    $form['#attached']['library'][] = 'group_member_import/default-design';

    return $form;

  }

  /**
   * Validate the file upload.
   */
  public static function validateFileupload(&$element, FormStateInterface $form_state, &$complete_form) {

    $validators = [
      'file_validate_extensions' => ['csv'],
    ];



    // @TODO: File_save_upload will probably be deprecated soon as well.
    // @see https://www.drupal.org/node/2244513.
    if ($file = file_save_upload('csvfile', $validators, FALSE, 0, FileSystemInterface::EXISTS_REPLACE)) {

      // The file was saved using file_save_upload() and was added to the
      // files table as a temporary file. We'll make a copy and let the
      // garbage collector delete the original upload.
      $csv_dir = 'temporary://csvfile';
      $directory_exists = \Drupal::service('file_system')
        ->prepareDirectory($csv_dir, FileSystemInterface::CREATE_DIRECTORY);

      if ($directory_exists) {
        $destination = $csv_dir . '/' . $file->getFilename();
        if (file_copy($file, $destination, FileSystemInterface::EXISTS_REPLACE)) {
          $form_state->setValue('csvupload', $destination);
        }
        else {
          $form_state->setErrorByName('csvimport', t('Unable to copy upload file to @dest', ['@dest' => $destination]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $delimiter = $form_state->getValue('delimiter');
    $csvupload = $form_state->getValue('csvupload');
    $csvtext = $form_state->getValue('csvcp');


    if (!isset($csvupload) && empty($csvtext)) {
      $form_state->setErrorByName('importdata', $this->t('You have to provide a text or upload a file!')); 
    }
   
    if (!isset($csvupload) || empty($csvtext)) {
      return;
    }


    if (!$csvupload && !$csvtext) {
      $form_state->setErrorByName('csvcp', $this->t('You have to provide a text or upload a file!'));    
    }
    
    if ($delimiter == '0') {
      $delimiter = ';';
    } 
    elseif ($delimiter == '1') {
      $delimiter = ',';
    }
    else {
      $delimiter = '	';
    }

    $group_member_import_fields = new GroupMemberImportFields();
    $availableFields = $group_member_import_fields->getAllAvailableFields();

    // Check if fields are allowed. 
    if ($csvtext) {      

      $headers = explode(PHP_EOL, $csvtext)[0];
      $line = explode($delimiter, $headers); // Explode via tab.
      
      foreach ($line as $field) {

        $field = trim($field);

        // Remove array index from fieldname for checking.
        if (substr_count($field, '.') > 1) {          
          $field_array = explode('.', $field);
          $field = $field_array[0] . '.' . $field_array[1];
        }
        
        if (!in_array($field, $availableFields)) {              
          $form_state->setErrorByName('csvcp', $this->t('Invalid field: ' . $field));
        }
      }

    } 
    else {

      if ($handle = fopen($csvupload, 'r')) {
        
        if ($headers = fgetcsv($handle, 4096, $delimiter)) {                                      

          foreach($headers as $field) {

            // Remove array index from fieldname for checking.
            if (substr_count($field, '.') > 1) {
              $field_array = explode('.', $field);
              $field = $field_array[0] . '.' . $field_array[1];
            }

            // Remove non-printable characters.
            $field = preg_replace('/[^a-zA-Z0-9_ -.]/s','',$field);                       

            if (!in_array($field, $availableFields)) {              
              $form_state->setErrorByName('csvfile', $this->t('Invalid field: ' . $field));
            }
          }

        }
        fclose($handle);
      }
      else {
        $form_state->setErrorByName('csvfile', $this->t('Unable to read uploaded file @filepath', ['@filepath' => $csvupload]));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $group_member_import_fields = new GroupMemberImportFields();
    $availableFields = $group_member_import_fields->getAllAvailableFields();
    $active_group_roles = [];
    $active_user_roles = [];
    $newLine = [];
    $header = null;
    $i = 0;

    $config = \Drupal::service('config.factory')->getEditable('group_member_import.settings');
    // Save status of overwrite passwords.
    $config->set('overwrite_passwords', $form_state->getValue('overwrite_passwords'));
    $config->save();

    if ($form_state->getValue('enrol')) {
      $group = $form_state->getValue('group');   
    } 
    else {
      $group = null;
    }
    
    $group_roles = $form_state->getValue('group_roles');
    foreach($group_roles as $key => $role) {
      if ($key === $role) {
        $active_group_roles[$key] = $key;
      }
    }

    $user_roles = $form_state->getValue('user_roles');
    if (isset($user_roles)) {
      foreach($user_roles as $key => $role) {
        if ($key === $role) {
          $active_user_roles[$key] = $key;
        }
      }
    }
  

    $delimiter = $form_state->getValue('delimiter');

    if ($delimiter == '0') {
      $delimiter = ';';
    } 
    elseif ($delimiter == '1') {
      $delimiter = ',';
    }
    else {
      $delimiter = '	';
    }

    $batch = [
      'title'            => $this->t('Importing CSV ...'),
      'operations'       => [],
      'init_message'     => $this->t('Commencing'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message'    => $this->t('An error occurred during processing'),
      'finished'         => '\Drupal\group_member_import\Batch\GroupMemberImportBatch::csvimportImportFinished'
    ];

    // Import from text.
    if ($csvtext = $form_state->getValue('csvcp')) {

      $lines = explode(PHP_EOL, $csvtext);      
      
      foreach ($lines as $key => $line) {

        if ($line) {

          // Replace tabs with delimiters.
          $line = trim($line);
          $line = explode($delimiter, $line);

          if ($key == 0) {
            $line_headers = $line;
          } 
          
          $batch['operations'][] = [
            '\Drupal\group_member_import\Batch\GroupMemberImportBatch::csvimportImportLine',
            [array_map('base64_encode', $line),$group, $active_group_roles, $active_user_roles, $line_headers]
          ];

        }        
      }


    // Import from file.
    } 
    elseif ($csvupload = $form_state->getValue('csvupload')) {

      // BOM as a string for comparison.
      $bom = "\xef\xbb\xbf";

      if ($handle = fopen($csvupload, 'r')) {

        // Progress file pointer and get first 3 characters to compare to the BOM string.
        if (fgets($handle, 4) !== $bom) {
          // BOM not found - rewind pointer to start of file.
          rewind($handle);
        }

        $batch['operations'][] = [
          '\Drupal\group_member_import\Batch\GroupMemberImportBatch::csvimportRememberFilename',
          [$csvupload]
        ];

        
        while ($line = fgetcsv($handle, 4096, $delimiter)) { 
          
          foreach ($availableFields as $fields) {
            
            foreach ($line as $lineFields) {

              if ($fields == $lineFields) {

                $line_headers = $line;

              }

            }

          }       
          
          // Use base64_encode to ensure we don't overload the batch
          // processor by stuffing complex objects into it.                    
          $batch['operations'][] = [
            '\Drupal\group_member_import\Batch\GroupMemberImportBatch::csvimportImportLine',
            [array_map('base64_encode', $line),$group, $active_group_roles, $active_user_roles, $line_headers]
          ];

        }         

        fclose($handle);

      }
    }
    
    batch_set($batch);
  }


    /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, GroupInterface $group = NULL) {
    // Check permissions and combine that with any custom access checking needed. Pass forward
    // parameters from the route and/or request as needed.

    $user = User::load($account->id());
      
    if ($group) {      
      $permission = $group->hasPermission('edit group', $user);    
      if ($permission) {            
        return AccessResult::allowed();
      }
    } 

    return AccessResult::forbidden();

  }

    /**
   * Sanitize the raw content string. Currently supported sanitizations:
   *
   * - Remove BOM header from UTF-8 files.
   *
   * @param string $raw
   *   The raw content string to be sanitized.
   * @return
   *   The sanitized content as a string.
   */
  public function sanitizeRaw($raw) {
    if (substr($raw, 0,3) == pack('CCC',0xef,0xbb,0xbf)) {
      $raw = substr($raw, 3);
    }
    return $raw;
  }


}
