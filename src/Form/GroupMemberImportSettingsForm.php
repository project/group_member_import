<?php
/**
 * @file
 * Contains \Drupal\group_member_import\Form\GroupMemberImportSettingsForm.
 */

namespace Drupal\group_member_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Drupal\group_member_import\GroupMemberImportFields;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Provides the settings form.
 */
class GroupMemberImportSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'group_member_import.settings';  

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_member_import_settings_form';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }  


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);

    $group_member_import_fields = new GroupMemberImportFields();
    $fields = $group_member_import_fields->getAllAvailableFieldsPerEntity();  

    $default_values_fields = $config->get('active_fields');
    $default_values_allowed_roles =  $config->get('allowed_roles');

    $form['label'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Check all fields you do not want to import.'),
    ];

    if (is_array($fields) && !empty($fields)) {
     
      foreach ($fields as $key => $field) {

        if (is_array($field) && !empty($field)) { 
          
          $options = array_combine($field, $field);
          
          $form[$key] = array(
            '#type' => 'details',
            '#title' => $key,
            '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
          );

          $form[$key]['check'] = array(
            '#type' => 'checkboxes',
            '#title' => $key,
            '#options' => $options,
            '#default_value' => $default_values_fields
          );

        }
      }
    }

    $allowed_roles = \Drupal::entityQuery('user_role')->accessCheck(FALSE)->execute();
    foreach($allowed_roles as $key => $role) {
      if ($key === 'administrator' || $key === 'authenticated' || $key === 'anonymous') {
        unset($allowed_roles[$key]);
      }
    }

    $form['allowed_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Allowed Roles'),
      '#options' => $allowed_roles,
      '#default_value' => $default_values_allowed_roles
    );

    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Save'),
    ];

    $form['#tree'] = TRUE;

 

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $config = $this->config(static::SETTINGS);    

    $values = $form_state->cleanValues()->getValues();
    $allowed_roles = $form_state->cleanValues()->getValue('allowed_roles'); 

    // Delete all current roles, before adding new ones
    $config->set('allowed_roles', []);

    foreach ($allowed_roles as $key => $value) {
      if ($key === $value) {
        $save_roles[$key] = $key;
        $config->set('allowed_roles', $save_roles);
        $config->save();
      }
    }

    foreach ($values as $key => $value) {

      if (is_array($value) & $key != 'allowed_roles') {

        foreach ($value['check'] as $index => $checked) {

          if ($index === $checked) {
            $save_fields[] = $index;
            $config->set('active_fields', $save_fields);
            $config->save();
          }

        }

      }

    }

    






  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, GroupInterface $group = NULL) {
    // Check permissions and combine that with any custom access checking needed. Pass forward
    // parameters from the route and/or request as needed.

    $user = User::load($account->id());

    //kint($group);

    if ($group) {

      $member = $group->getMember($account);

      if ($member) {
        if($member->hasPermission('edit group', $account)) {
          return AccessResult::allowed();
        }
      }
      elseif ($user->hasRole('administrator')) {
        return AccessResult::allowed();
      }

    }
    else {
      if ($user->hasRole('administrator')) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();

  }


}
