<?php

namespace Drupal\group_member_import\Plugin\rest\resource;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\group_member_import\GroupMemberImportFields;

/**
 * Represents Rest Member Import records as resources.
 *
 * @RestResource (
 *   id = "group_member_import_rest_member_import",
 *   label = @Translation("Rest Member Import"),
 *   uri_paths = {
 *     "create" = "/api/group-member-import-rest-member-import"
 *   }
 * )
 *
 * @DCG
 * The plugin exposes key-value records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. An
 * example of such configuration can be located in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can enable it through admin interface provider by REST UI
 * module.
 * @see https://www.drupal.org/project/restui
 *
 * @DCG
 * Notice that this plugin does not provide any validation for the data.
 * Consider creating custom normalizer to validate and normalize the incoming
 * data. It can be enabled in the plugin definition as follows.
 * @code
 *   serialization_class = "Drupal\foo\MyDataStructure",
 * @endcode
 *
 * @DCG
 * For entities, it is recommended to use REST resource plugin provided by
 * Drupal core.
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
class RestMemberImportResource extends ResourceBase {

  /**
   * The key-value storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    KeyValueFactoryInterface $keyValueFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $keyValueFactory);
    $this->storage = $keyValueFactory->get('group_member_import_rest_member_import');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('keyvalue')
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param array $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post(array $data = ['error' => 'error']) {

    $group_member_import_fields = new GroupMemberImportFields();
    
    // Get available fields
    $availableFields = $group_member_import_fields->getAllAvailableFields();

    $userArray = [];
    $profileArray = []; 


    try {

      if (isset($data['error'])) {
        $this->logger->warning("Data can't be empty.");
        throw new NotFoundHttpException("Data can't be empty.");
      }

      if (!isset($data['group'])) {
        $this->logger->warning("Group can't be empty.");
        throw new NotFoundHttpException("Group can't be empty.");
      }

      if (!isset($data['group_roles'])) {
        $this->logger->warning("Group roles can't be empty.");
        throw new NotFoundHttpException("Group roles can't be empty.");
      } 
      
      if (!isset($data['user_roles'])) {
        $this->logger->warning("User roles can't be empty.");
        throw new NotFoundHttpException("User roles can't be empty.");
      }

      if (!isset($data['import_data']) || !is_array($data['import_data']) || empty($data['import_data'])) {
        $this->logger->warning("Import data can't be empty.");
        $this->logger->warning('<pre><code>' . print_r($data['import_data'], TRUE) . '</code></pre>');
        throw new NotFoundHttpException("Import data can't be empty.");
      }

      // Get group
      $group = $data['group'];

      // Get group roles
      $group_roles = $data['group_roles'];

      // Get user roles
      $user_roles = $data['user_roles'];


      $import_data = $data['import_data'][0];

      $this->logger->warning('ID<pre><code>' . print_r($import_data , TRUE) . '</code></pre>');


      // Validate fields
      foreach ($import_data as $key => $value) {
        $field_value = $key;

        $this->logger->warning('ID<pre><code>' . print_r($field_value , TRUE) . '</code></pre>');

        // Remove array index from fieldname for checking.
        if (substr_count($field_value, '.') > 1) {          
          $field_array = explode('.', $field_value);
          $field_value = $field_array[0] . '.' . $field_array[1];
        }
        
        if (!in_array($field_value, $availableFields)) {    
          $invalid_fields[] = $field_value;          
        }        

      }

      if (isset($invalid_fields) && !empty($invalid_fields)) {

        $invalid_fields_text = implode(", ", $invalid_fields);

        $this->logger->warning('Invalid fields detected: ' . $invalid_fields_text);
        throw new NotFoundHttpException('Invalid fields detected. Fields:' . $invalid_fields_text);
      
      }

      foreach ($import_data as $field => $field_data) {
    

        // Get the field info
        $field_info = explode('.', $field);
        
        // Get the bundle
        $bundle = $field_info[0];
        
        // Get the field name
        $fieldName = $field_info[1];

        if (array_key_exists(2, $field_info)) {
          $fieldArrayIndex = $field_info[2];
        } 
        else {
          $fieldArrayIndex = null;
        }

        $field_type = $group_member_import_fields->getFieldType($bundle,$fieldName);

        //Check any timezone field before running through field_types
        if ($fieldName === 'timezone') {

          if (!$this->isValidTimezone($field_data)) {
            $timezone = $timezone_default;
          }
          else {
            $timezone = $field_data;
          }

          if ($bundle === 'user') {
            $userArray[$fieldName] = $timezone;
          }
          else {
            $profileArray[$bundle][$fieldName] = $timezone;
          }

        }        
        
        switch($field_type) {          

          // Support links.
          case 'link':

            // Cleanup links.
            if ($field_data) {              
              if (!filter_var($field_data, FILTER_VALIDATE_URL)) {              
                $field_data = 'https://' . trim($field_data);
              }
              if (!filter_var($field_data, FILTER_VALIDATE_URL)) {                              
                $valuesarray = array();
              } else {                 
                $valuesarray['uri'] = $field_data;
                $valuesarray['title'] = $field_data;
                $valuesarray['options'] = array();
              }                         
            } else {
              $valuesarray = array();              
            }            

            $field_data = $valuesarray;
            
            
            if ($bundle === 'user') {
              $userArray[$fieldName] = $field_data;
            } else {
              $profileArray[$bundle][$fieldName] = $field_data;
            }             

            break;

          // Support address fields.
          case 'address':
            if ($fieldArrayIndex) {
              if ($bundle === 'user') {
                $userArray[$fieldName] = [$fieldArrayIndex => $field_data];
              } else {
                $profileArray[$bundle][$fieldName][$fieldArrayIndex] = $field_data;
              } 
            } else {
              \Drupal::logger('group_member_import')->notice('Missing array index for address field - skipping address information.');
            }            
            break;

          case 'entity_reference':

            $field_settings = $group_member_import_fields->getFieldSettings($bundle,$fieldName);
            if ($field_settings['target_type'] == 'taxonomy_term') {
              $target_bundles = $field_settings['handler_settings']['target_bundles'];

              // If vocabulary field settings target is single, assume it.
              if (count($target_bundles) == 1 && !empty($field_data)) {
                $terms = $group_member_import_fields->getTermReference($target_bundles[key($target_bundles)], $field_data, $fieldArrayIndex, true);
              }

              // If not, assume vocabulary is added with ":" delimiter.
              else {
                $reference = explode(":", $field_data);
                if (is_array($reference) && $reference[0] != '') {
                  $terms = $group_member_import_fields->getTermReference($reference[0], $reference[1], $fieldArrayIndex, false);
                }
              }
              if (!empty($terms)) {

                if ($bundle === 'user') {
                  $userArray[$fieldName] = $terms;
                }
                else {
                  $profileArray[$bundle][$fieldName] = $terms;
                }                
              }
            }
            elseif ($field_settings['target_type'] == 'user') {
              $userImportArray = explode(', ', $field_data);
              $users = $group_member_import_fields->get_user_info($userImportArray);              

              if ($bundle === 'user') {
                $userArray[$fieldName] = $users;
              }
              else {
                $profileArray[$bundle][$fieldName] = $users;
              } 

            }
            elseif ($field_settings['target_type'] == 'node') {
              $nodeImportArrays = explode(':', $field_data);
              $nodeReference1 = $group_member_import_fields->get_node_id($nodeImportArrays);

              if ($bundle === 'user') {
                $userArray[$fieldName] = $nodeReference1;
              }
              else {
                $profileArray[$bundle][$fieldName] = $nodeReference1;
              } 

            }

            break;

          case 'datetime':
            $dateArray = explode(':', $field_data);
            if (count($dateArray) > 1) {
              $dateTimeStamp = strtotime($field_data);
              $newDateString = date('Y-m-d\TH:i:s', $dateTimeStamp);
            }
            else {
              $dateTimeStamp = strtotime($field_data);
              $newDateString = date('Y-m-d', $dateTimeStamp);
            }

            if ($bundle === 'user') {
              $userArray[$fieldName] = ["value" => $newDateString];
            }
            else {
              $profileArray[$bundle][$fieldName] = ["value" => $newDateString];
            } 
              
            break;

          case 'timestamp':

            if ($bundle === 'user') {
              $userArray[$fieldName] = ["value" => $field_data];
            }
            else {
              $profileArray[$bundle][$fieldName] = ["value" => $field_data];
            }                 
                
            break;

          case 'language':  

            if (!$this->isValidLangcode($field_data)) {
              $langcode = $language_default;
            }
            else {
              $langcode = $field_data;
            }
            
            if ($bundle === 'user') {
              $userArray[$fieldName] = $langcode;
            }
            else {
              $profileArray[$bundle][$fieldName] = $langcode;
            } 
          
            break;

          case 'list_string':
            
            $listArray = explode(",",$field_data);
            array_walk($listArray, 'trim');

            if ($bundle === 'user') {
              $userArray[$fieldName] = $listArray;
            }
            else {
              $profileArray[$bundle][$fieldName] = $listArray;
            } 

            break;

          case 'authored_by':
            $user_id = $group_member_import_fields->get_user_id($field_data);


            if ($bundle === 'user') {
              $userArray[$fieldName] = ($user_id > 0) ? $user_id : \Drupal::currentUser()->id();
            }
            else {
              $profileArray[$bundle][$fieldName] = ($user_id > 0) ? $user_id : \Drupal::currentUser()->id();
            }            

            break;

          case 'email':
            $email_value = str_replace(' ', '', $field_data);
            if (!$this->validateEmail($email_value)) {
              $context['results']['failed_rows'][] = $line;
            }

            if ($bundle === 'user') {
              $userArray[$fieldName] = $email_value;
            }
            else {
              $profileArray[$bundle][$fieldName] = $email_value;
            }    

          case 'boolean':
            if ($bundle === 'user' && $field_name === 'status') {
              // Set the status only if valid
              if ($this->validateStatus($field_value)) {
                $userArray[$fieldName] = $field_value;
              }              
            }
            elseif ($bundle === 'user' && $field_name != 'status') {
              $userArray[$fieldName] = $field_value;
            }
            else {
              $profileArray[$bundle][$fieldName] = $field_value;
            }
            

          default:            
                      
            if ($bundle === 'user') {
              $userArray[$fieldName] = $field_data;
            }
            else {
              $profileArray[$bundle][$fieldName] = $field_data;
            }  

            break;
          
        }        


      }


      $importUser = $group_member_import_fields->importUser($userArray,$profileArray,$group, $group_roles, $user_roles);

      $response_data = [
        'data' => $import_data,
        'success' => TRUE,
      ];

      $response = new ModifiedResourceResponse($response_data, 201);
      

    } 
    catch (\Exception $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();

      $response_data = [
        'data' => $e->getMessage(),
        'success' => FALSE,
      ];

      $response = new ModifiedResourceResponse($error, 400);
    
    }
    
    // Return the newly created record in the response body.
    return $response;

  }



  protected function isValidLangcode($langcode) {

    $allowed_languages = \Drupal::languageManager()->getLanguages();

    if(array_key_exists($langcode,$allowed_languages)) {
      return true;
    }

    return false;

  }

  protected function isValidTimezone($timezone) {

    $timezones = User::getAllowedTimezones();   

    if (in_array($timezone, $timezones)) {
      return true;
    }

    return false;

  }



  protected function validateEmail(string $email) {
    if (\Drupal::service('email.validator')->isValid($email)) {
      return true;
    }
    return false;
  }

  protected function validateStatus(string $status) {
    if ($status === "1" || $status === "0") {
      return true;
    }
    return false;
  }

  protected function cleanPassword(string $password) {
    return str_replace(' ','', $password);
  }  

}
